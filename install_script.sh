#!/bin/sh

confirm() {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case $response in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

sudo apt-get install -y software-properties-common

PPA_LIST=(ppa:weather-indicator-team/ppa ppa:philip.scott/elementary-tweaks ppa:numix/ppa ppa:snwh/pulp ppa:snwh/ppa ppa:webupd8team/sublime-text-3 ppa:linrunner/tlp ppa:noobslab/themes ppa:noobslab/icons ppa:starws-box/deadbeef-player ppa:libreoffice/ppa ppa:noobslab/apps ppa:moka/stable ppa:captiva/ppa ppa:landronimirc/skippy-xd-daily ppa:varlesh-l/papirus-pack)

PKG_LIST="curl git wget python python3 python-pip guake firefox browser-plugin-vlc numix-gtk-theme \
      numix-icon-theme numix-icon-theme-circle paper-gtk-theme paper-icon-theme \
      flatabulous-theme ultra-flat-icons tlp tlp-rdw tp-smapi-dkms acpi-call-dkms powertop vim \
      synaptic unp unar unrar rar sublime-text-installer toilet cowsay autoconf \
      automake autogen pkg-config zsh tcsh libgtk-3-dev gnome-themes-standard gtk2-engines-* \
      redshift redshift-gtk deadbeef moka-icon-theme captiva-icon-theme skippy-xd \
      synaptic papirus-gtk-icon-theme indicator-weather"

for f in ${PPA_LIST[@]}; do 
  sudo add-apt-repository -y $f; 
done

sudo apt-get update
sudo apt-get install -y ${PKG_LIST}

PIP_LIST="cheat"
sudo pip install ${PIP_LIST}

confirm "install xdgurl for gnome-look.org? [n/Y]" && {
  curl -o xdgurl.deb https://dl.opendesktop.org/api/files/download/id/1468612049/xdgurl_1.0.1-0ubuntu1_amd64.deb && sudo dpkg -i xdgurl.deb
  rm -f xdgurl.deb
  sudo apt-get install -f
}

echo "install chrome for 64bit or chromium?"
select choice in Chrome Chromium None; do
  case $choice in
    Chrome)
      cd ~/Downloads
      curl -o chrome.deb https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && sudo dpkg -i chrome.deb;
      rm -f chrome.deb
      sudo apt-get install -f
      break;;

    Chromium) 
      sudo apt-get install -y chromium-browser chromium-codecs-ffmpeg chromium-chromedriver flashplugin-installer; 
      break;;

    None) 
      break;;
  esac
done

installDolphin() {
  sudo apt-get install -y dolphin kde-service-menu-fuseiso kdenetwork-filesharing \ 
        kdegraphics-thumbnailers kdesdk-thumbnailers kdemultimedia-mplayerthumbs \
        kde-thumbnailer-deb kde-thumbnailer-openoffice dolphin-plugins
}

installThunar() {
  sudo apt-get install -y thunar thunar-archive-plugin thunar-media-tags-plugin \
        thunar-volman thunar-thumbnailers thunar-vcs-plugin thunar-dropbox-plugin
}

installNautilus() {
  sudo apt-get install -y nautilus nautilus-actions nautilus-admin nautilus-compare nautilus-dropbox \
        nautilus-emblems nautilus-gtkhash nautilus-ideviceinfo nautilus-image-manipulator \
        nautilus-sendto nautilus-share
}

installThumbnailPlugins() {
  sudo apt-get install -y poppler-glib ffmpegthumbnailer freetype2 libgsf raw-thumbnailer totem evince tumbler
} 

echo "install tweak tool?"
select choice in GNOME Elementary none; do
  case $choice in
    GNOME)
      sudo apt-get install -y gnome-tweak-tool;
      break;;
    Elementary)
      sudo add-apt-repository -y ppa:mpstark/elementary-tweaks-daily
      sudo apt-get update && sudo apt-get install -y elementary-tweaks
      break;;
    none)
      break;;
  esac
done

confirm "install plank? [y/N]" && {
  sudo add-apt-repository ppa:docky-core/stable
  sudo add-apt-repository ppa:noobslab/apps
  sudo apt-get install -y plank plank-themer plank-theme-*
  cd /tmp/ && .Replace.sh;cd
}

echo "install file manager? (Thunar can't share files with windows easily)"
select choice in "Dolphin (KDE)" "Thunar (XFCE)" "Nautilus (GNOME)" all none; do
  case $choice in
    Dolphin*)
      installDolphin
      installThumbnailPlugins
      break;;

    Thunar*)
      installThunar
      installThumbnailPlugins
      break;;
      
    Nautilus*)
      installNautilus
      installThumbnailPlugins
      break;;

    all)
      installDolphin
      installThunar
      installNautilus
      installThumbnailPlugins
      break;;

    none)
      break;;
  esac
done

confirm "install libre office with extensions? [y/N]" && {
  sudo apt-get install -y libreoffice libreoffice-gnome libreoffice-kde libreoffice-pdfimport \
      libreoffice-presentation-minimizer libreoffice-presenter-console mozilla-libreoffice \
      libreoffice-gtk libreoffice-gtk-gnome libreoffice-gtk3 libreoffice-style-sifr \
      libreoffice-style-crystal libreoffice-style-galaxy \
      libreoffice-style-human libreoffice-style-oxygen libreoffice-style-tango libreoffice-style-breeze \
      libreoffice-templates 
}


confirm "install dockbarx for xfce4-panel?" [y/N] && {
  sudo add-apt-repository -y ppa:dockbar-main/ppa && sudo apt-get update
  sudo apt-get install -y --no-install-recommends xfce4-dockbarx-plugin dockbarx-themes-extra
}

confirm "install kWin-tiling? [y/N]" && {
  cd ~/Downloads
  git clone https://github.com/faho/kwin-tiling.git
  cd kwin-tiling/
  plasmapkg2 --type kwinscript -i .
}


confirm "install Oracle JDK 8? [y/N]" && {
  sudo add-apt-repository -y ppa:webupd8team/java && sudo apt-get update
  sudo apt-get install -y oracle-java8-installer oracle-java8-set-default
}

confirm "install themes from github? [y/N]" && {
  echo "installing arc-flatabulous-theme"  
  cd ~/Downloads 
  git clone https://github.com/andreisergiu98/arc-flatabulous-theme --depth 1 
  cd arc-flatabulous-theme 
  ./autogen.sh --prefix=/usr
  sudo make install
  rm -rf ../arc-flatabulous-theme

  echo "installing arc-firefox-theme"
  cd ~/Downloads
  git clone https://github.com/horst3180/arc-firefox-theme --depth 1 
  cd arc-firefox-theme
  ./autogen.sh --prefix=/usr
  sudo make install
  rm -rf ../arc-firefox-theme

  echo "installing adapta gtk theme"
  cd ~/Downloads
  sudo rm -rf /usr/share/themes/{Adapta,Adapta-Nokto}
  rm -rf ~/.local/share/themes/{Adapta,Adapta-Nokto}
  rm -rf ~/.themes/{Adapta,Adapta-Nokto}
  git clone https://github.com/tista500/Adapta --depth 1
  cd Adapta
  ./autogen.sh --prefix=/usr --enable-chrome --enable-plank --enable-gtk_next
  make
  sudo make install
  rm -rf ../Adapta

  echo "installing arc gtk theme"
  cd ~/Downloads
  sudo rm -rf /usr/share/themes/{Arc,Arc-Darker,Arc-Dark}
  rm -rf ~/.local/share/themes/{Arc,Arc-Darker,Arc-Dark}
  rm -rf ~/.themes/{Arc,Arc-Darker,Arc-Dark}
  git clone https://github.com/horst3180/arc-theme --depth 1 
  cd arc-theme
  ./autogen.sh --prefix=/usr 
  sudo make install
  rm -rf ../arc-theme
 
  echo "installing Panther-OSX theme"
  cd ~/Downloads
  sudo rm -rf /usr/share/themes/Panther-OSX
  curl -O https://dl.opendesktop.org/api/files/download/id/1470523664/Panther-OSX.tar.gz
  unp Panther-OSX.tar.gz
  rm Panther-OSX.tar.gz
  sudo mv Panther-OSX /usr/share/themes
  
  echo "installing Panthera-OSX theme"
  cd ~/Downloads
  sudo rm -rf /usr/share/themes/Panthera-OSX
  curl -O https://dl.opendesktop.org/api/files/download/id/1470657741/Panthera-OSX.tar.gz
  unp Panthera-OSX.tar.gz
  rm Panthera-OSX.tar.gz
  sudo mv Panthera-OSX /usr/share/themes
  
  echo "installing Candy theme"
  cd ~/Downloads
  sudo rm -rf /usr/share/themes/Candy
  curl -o Candy.zip https://dl.opendesktop.org/api/files/download/id/1470620798/Candy%20Theme%20v0.2.zip
  unp Candy.zip
  rm Candy.zip
  sudo mv Candy /usr/share/themes
  
  echo "installing Flat-Adapta-OSX theme"
  cd ~/Downloads
  sudo rm -rf /usr/share/themes/Flat-Adapta-OSX
  curl -o Flat-Adapta-OSX.tar.gz https://dl.opendesktop.org/api/files/download/id/1469477473/Flat-Adapta-OSX-1.0-stable.tar.gz
  unp Flat-Adapta-OSX.tar.gz
  rm Flat-Adapta-OSX.tar.gz
  sudo mv Flat-Adapta-OSX /usr/share/themes
  
  echo "installing OSX-Breeze theme"
  cd ~/Downloads
  sudo rm -rf /usr/share/themes/OSX-Breeze
  curl -O https://dl.opendesktop.org/api/files/download/id/1468362436/OSX-Breeze.zip
  unp OSX-Breeze.zip
  rm OSX-Breeze.zip
  sudo mv OSX-Breeze /usr/share/themes
  
  echo "installing OSX-White theme"
  cd ~/Downloads
  sudo rm -rf /usr/share/themes/OSX-White
  curl -o OSX-White.zip https://dl.opendesktop.org/api/files/download/id/1464728456/175940-OSX-White.zip
  unp OSX-White.zip
  rm OSX-White.zip
  sudo mv OSX-White /usr/share/themes
  
  echo "installing La Capitaine icons"
  cd /usr/share/icons
  git clone https://github.com/keeferrourke/la-capitaine-icon-theme.git
  
  echo "installing Captiva icons"
  cd ~/Downloads
  curl -o Captiva.zip https://github.com/captiva-project/captiva-icon-theme/archive/master.zip
  unp Captiva.zip
  rm Captiva.zip
  sudo mv Captiva /usr/share/icons
  
  echo "installing Uniform icons"
  cd ~/Downloads
  curl -o Uniform.zip https://downloader.disk.yandex.com/disk/1bb6960e424d3ce7d8ca635eaa8bcb511bec5271bc0e9556db8c9a473e7d9aeb/57ad0d78/LrLIAmix5hqiqjtvOniV8AAjfO99KA77L6Q16Iq9LXm5H9kubbSrtcAVtRkeo-ydJJ7gezs7rcvivcNawF7GOQ%3D%3D?uid=0&filename=Uniform%2B%20icons.tar.gz&disposition=attachment&hash=n57HW2QMG7wVueBWhOLhDhM%2BEx/bPJXcXwv%2Bde9a1I0%3D&limit=0&content_type=application%2Fgzip&fsize=83140105&hid=2d3e8a3591e56bb031680e32b881afeb&media_type=compressed&tknv=v2
  unp Uniform.zip
  rm Uniform.zip
  sudo mv Uniform/* /usr/share/icons
  rm -r Uniform
  
  echo "installing Mato icons"
  sh -c 'cd /tmp; rm -rf Mato/; git clone https://github.com/flipflop97/Mato.git; Mato/install.sh'
  
  echo "installing charmander theme and icons"
  cd ~/Downloads
  git clone https://github.com/btd1337/Stupendously-Hot-Charmander
  cd Stupendously-Hot-Charmander
  sudo chmod +x installer.sh
  ./installer.sh
  cd .. && rm -rf Stupendously-Hot-Charmander
  
}

confirm "install android studio? [y/N]" && {
  sudo add-apt-repository -y ppa:paolorotolo/android-studio && sudo apt-get update
  sudo apt-get install -y android-studio
  #cd ~/Downloads
  #curl -O https://dl.google.com/dl/android/studio/ide-zips/2.1.1.0/android-studio-ide-143.2821654-linux.zip
  #unp android-studio-ide-*.zip
  #cd android-studio-ide-*
  #./studio.sh
}

#echo "install franz messager for skype, facebook etc?"
#select choice4 in "yes" "no"; do
#  case $yn4 in
#    yes) 
#      curl -O http://bit.ly/1VhTiUa | unp 
#      break;;
#
#    no) 
#      break;;
#  esac
#done

confirm "calibrate powertop? [y/N] (laptop HAS to be on battery)" && sudo powertop --calibrate
sudo tlp start
sudo powertop --auto-tune
guake &
